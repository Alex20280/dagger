package layouts.sourceit.com.dependenciesinjection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import javax.inject.Inject;

import layouts.sourceit.com.dependenciesinjection.App.App;
import layouts.sourceit.com.dependenciesinjection.nerwork.NetworkUtils;
import layouts.sourceit.com.dependenciesinjection.storage.DatabaseHelper;

public class MainActivity extends AppCompatActivity {

    @Inject
    DatabaseHelper databaseHelper;

    @Inject
    NetworkUtils networkUtils;

    //MainActivityPresenter mainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        databaseHelper = App.getComponent().getDataBaseHelper();
//        networkUtils = App.getComponent().getnetworkUtils();

        App.getComponent().injectMainActivity(this);

    }
}
