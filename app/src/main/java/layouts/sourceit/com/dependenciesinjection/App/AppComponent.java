package layouts.sourceit.com.dependenciesinjection.App;

import dagger.Component;
import layouts.sourceit.com.dependenciesinjection.MainActivity;
import layouts.sourceit.com.dependenciesinjection.nerwork.NetworkModule;
import layouts.sourceit.com.dependenciesinjection.nerwork.NetworkUtils;
import layouts.sourceit.com.dependenciesinjection.storage.DatabaseHelper;
import layouts.sourceit.com.dependenciesinjection.storage.StorageModule;

@Component (modules = {StorageModule.class, NetworkModule.class})
public interface AppComponent {
//    NetworkUtils getnetworkUtils();
//    DatabaseHelper getDataBaseHelper();
    void injectMainActivity(MainActivity mainActivity);
}
